function on(name) {
    document.getElementById("overlay").style.display = "block";
    getContent(name);
}

function off() {
    document.getElementById("overlay").style.display = "none";
}

function getContent(name){
	if(name==="workshop1"){
		document.getElementById("heading").innerHTML="<p><b><i>Hyundai Workshop</i></b></p>";
		document.getElementById("context").innerHTML="<p>INDCON'18 brings you a chance to experience the making of the automotive brilliance with HYUNDAI. A lecture session followed by an Industrial Visit to the India's only Hyundai manufacturing unit HYUNDAI MOTOR INDIA LIMITED, Sriperumbadur.<br/>Note: Lunch will be provided for the particiants.</p><br/><a href=\"https://www.townscript.com/e/hyundai-workshop-413011\" style:\"color: white; text-decoration: none;\"><div id=\"register\" align=\"center\" style=\"color:black;text-decoration: none;border:0.25vh solid black;border-radius:7%;width:40%;heigth:40%;\">Register</div></a>";
	}

	else if(name==="workshop2"){
		document.getElementById("heading").innerHTML="<p><b><i>Hyundai Workshop</i></b></p>";
		document.getElementById("context").innerHTML="<p>An opportunity of a lifetime to understand the journey of car manufacturing and falling in love with Hyundai's automotive brilliance.<br/> The workshop will be conducted</p><a href=\"https://www.townscript.com/e/urban-dance-workshop-003420\" style:\"color: white; text-decoration: none;\"><div id=\"register\" align=\"center\" style=\"color:black;text-decoration: none;border:0.25vh solid black;border-radius:7%;width:40%;heigth:40%;\">Register</div></a>";
	}
	else if(name==="workshop3"){
		document.getElementById("heading").innerHTML="<p><b><i>Business Storytelling</i></b></p>";
		document.getElementById("context").innerHTML="<p>To all the entrepreneurs and business fanatics, come and wallow in the captivating and informative lecture by India's number one behaviour scientist Mr. Yogesh Parmar.</p><a href=\"https://www.townscript.com/e/business-story-telling-244103\" style:\"color: white; text-decoration: none;\"><div id=\"register\" align=\"center\" style=\"color:black;text-decoration: none;border:0.25vh solid black;border-radius:7%;width:40%;heigth:40%;\">Register</div></a>";
	}
	else if(name==="innovate"){
		document.getElementById("heading").innerHTML="<p><b><i>I Innovate</i></b></p>";
		document.getElementById("context").innerHTML="<p>\"Innovation is seeing what everybody has seen and thinking what nobody has thought.\"<br/> Open up your creative minds and make the most exceptional product with the strangest things.</p>";
	}
	else if(name==="sellit"){
		document.getElementById("heading").innerHTML="<p><b><i>How will you sell it</i></b></p>";
		document.getElementById("context").innerHTML="<p>INDCON's most notabe management event that requires participants to sell to the judges few of the most craziest products. Take the challenge and be surprise your audience.</p>";
	}
	else if(name==="presentation"){
		document.getElementById("heading").innerHTML="<p><b><i>Paper Presentation</i></b></p>";
		document.getElementById("context").innerHTML="<p>Present your latest research, findings and ideas on the most trending themes in Industrial Engineering. Anyone with a unique perspective and approach to the topic are welcome to put forward their thoughts and perceptions.<br/><br/>Topic: Recent trends in Industrial Engineering Application.</p>";
	}
	else if(name==="egg"){
		document.getElementById("heading").innerHTML="<p><b><i>Save The Egg</i></b></p>";
		document.getElementById("context").innerHTML="<p>\"Humpty Dumpty sat on a wall wearing the engineered vest. He fell down once he fell down twice and he fell a one hundred times but all the kings horses and all the kings men needn't have to put humpty together ever again.\"<br/><br/>Make the most comfortable and safest vest for our dear old humpty and win exciting prizes.</p>";
	}
	else if(name==="robot"){
		document.getElementById("heading").innerHTML="<p><b><i>I-ROBOT</i></b></p>";
		document.getElementById("context").innerHTML="<p>If you are one of those robotic geeks, this is the event for you. But make sure your bot is up for the battle of the metal warriors.</p>";
	}
	else if(name==="worditout"){
		document.getElementById("heading").innerHTML="<p><b><i>Word It Out</i></b></p>";
		document.getElementById("context").innerHTML="<p>Make the most of your vocabulary and your language skills and participate in indcon's word challenge.</p>";
	}
	else if(name==="thunt"){
		document.getElementById("heading").innerHTML="<p><b><i>Treasure Hunt</i></b></p>";
		document.getElementById("context").innerHTML="<p>The X is marked and the ship has sailed, come back with the loot or come back not. Scrutinize and inspect every fragment of our college and grab exciting prizes.</p>";
	}
	else if(name==="igame"){
		document.getElementById("heading").innerHTML="<p><b><i>I Game</i></b></p>";
		document.getElementById("context").innerHTML="<p>To all the ardent video gamers, we have got the event for you. Unleash your gaming skills and have the best time with your friends.</p>";
	}
	else if(name==="iquiz"){
		document.getElementById("heading").innerHTML="<p><b><i>I Quiz</i></b></p>";
		document.getElementById("context").innerHTML="<p>To quiz the virtuosos of engineering on the latest science and technology.</p>";
	}
	else if(name==="mhunt"){
		document.getElementById("heading").innerHTML="<p><b><i>Material Hunt</i></b></p>";
		document.getElementById("context").innerHTML="<p>All you need is to be quick and vigilant and find the things that need to be found.</p>";
	}
	else if(name==="football"){
		document.getElementById("heading").innerHTML="<p><b><i>3's Football</i></b></p>";
		document.getElementById("context").innerHTML="<p>To all those passionate players whose hearts beat vigourously with every kick, spend a marvellous day with a ball at your feet and fire in your Eyes.</p>";
	}
}